package server;

import core.ComboServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ComboServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    public ComboServiceThread(ThreadGroup group, String name, Socket dataSocket)
    {
        super(group, name);

        try
        {
            this.dataSocket = dataSocket;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    @Override
    public void run()
    {
        String incomingMessage = "";
        String response;

        try
        {
            while(!incomingMessage.equals(ComboServiceDetails.END_SESSION))
            {
                response = null;
                incomingMessage = input.nextLine();
                System.out.println("Received message " + incomingMessage);

                String[] components = incomingMessage.split(ComboServiceDetails.COMMAND_SEPARATOR);

                CommandFactory factory = new CommandFactory();
                Command command = factory.createCommand(components[0]);

                if(command != null)
                {
                    response = command.createResponse(components);
                }
                else if(components[0].equals(ComboServiceDetails.END_SESSION))
                {
                    response = ComboServiceDetails.SESSION_TERMINATED;
                }
                else
                {
                    response = ComboServiceDetails.UNRECOGNISED;
                }

                //Send back the response
                output.println(response);
            }
        }
        //Need this for when Client closes and input stream is corrupted
        catch(NoSuchElementException nse)
        {
            System.out.println("Client closed unexpectedly " + nse.getMessage());
        }
        finally
        {
            try
            {
                dataSocket.close();
            }
            catch(IOException ioe)
            {
                System.out.println(ioe.getMessage());
                System.exit(1);
            }
        }
    }


}
