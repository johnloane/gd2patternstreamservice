package server;

import core.ComboServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ComboStreamServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        ThreadGroup group = null;
        try
        {
            listeningSocket = new ServerSocket(ComboServiceDetails.LISTENING_PORT);

            group = new ThreadGroup("Client threads");
            group.setMaxPriority(Thread.currentThread().getPriority() -1);

            //Do the main logic of the server
            boolean continueRunning = true;
            //int threadCount = 0;
            while(continueRunning)
            {
                dataSocket = listeningSocket.accept();
                ComboServiceThread newClient = new ComboServiceThread(group, dataSocket.getInetAddress() + "", dataSocket);
                newClient.start();

                System.out.println("The server has now accepted " + group.activeCount());
            }
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                //Will deal with dataSocket in the thread class
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println(ioe.getMessage());
                System.exit(1);
            }
        }
    }
}
