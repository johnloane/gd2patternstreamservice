package server;

import core.ComboServiceDetails;

public class CommandFactory
{
    public Command createCommand(String command)
    {
        Command c = null;

        if(command.equals(ComboServiceDetails.ECHO))
        {
            c = new EchoCommand();
        }
        else if(command.equals(ComboServiceDetails.DAYTIME))
        {
            c = new DaytimeCommand();
        }
        return c;
    }
}
