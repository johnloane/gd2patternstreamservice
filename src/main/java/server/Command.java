package server;

public interface Command
{
    public String createResponse(String[] components);
}
